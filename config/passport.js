const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');
const config = require('../config/database');

module.exports = function(passport){
  let opts = {};
  console.log("test1");
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
  //opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  opts.secretOrKey = config.secret;
  // opts.issuer = 'accounts.examplesoft.com';
  // opts.audience = 'yoursite.net';
  console.log("fromAuthHeader");
  console.log(opts);
  console.log("test2");
  passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
    console.log("test3");
    console.log(jwt_payload)
    User.getUserById(jwt_payload._id, (err, user) => {
      console.log("test4");
      if(err){
        return done(err, false);
      }
      if(user){
        return done(null, user);
      } else {
        return done(null, false);
      }
    });
  }));
}

